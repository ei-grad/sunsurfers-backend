from django.urls import path

from tgauth import views


app_name = "tgauth"
urlpatterns = [
    path("webhook/<token>", views.botapi_view, name="webhook"),
    path("login/<token>", views.login_view, name="login"),
]
