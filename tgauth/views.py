from uuid import uuid4
import json
import logging

from django.conf import settings
from django.urls import reverse
from django.http import Http404
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.contrib.auth import get_user_model, login, authenticate

from social_django.models import UserSocialAuth

from tgauth.auth import signer
from tgauth.utils import emojize

from surfers.models import LatestPoint

logger = logging.getLogger(__name__)


def user_get_or_create(data):

    try:
        social_user = UserSocialAuth.objects.get(
            uid=data["id"], provider="telegram"
        )
        user = social_user.user
        created = False
    except UserSocialAuth.DoesNotExist:
        username = data.get("username")
        if (
            not username
            or get_user_model().objects.filter(username=username).exists()
        ):
            username = "tg{}".format(data["id"])
        while get_user_model().objects.filter(username=username).exists():
            username = "tg{}_{}".format(data["id"], uuid4().hex)
        user = get_user_model()(
            username=username,
            first_name=data["first_name"],
            last_name=data.get("last_name", ""),
        )
        user.save()
        social_user = UserSocialAuth(
            user=user, uid=data["id"], provider="telegram", extra_data=data
        )
        social_user.save()
        created = True
        logger.info("Created user %s for telegram ID %s", username, data["id"])

    return user, created


def BotAPI(commands, secret_token):
    @csrf_exempt
    @require_http_methods(["POST"])
    def botapi(request, token):

        if not token or token != secret_token:
            raise Http404()

        update = json.loads(request.body.decode(request.encoding or "utf-8"))

        logger.info("Got update: %s", update)

        if "message" in update:

            msg = update["message"]

            if "from" not in msg:
                logger.error("Got message from without 'from': %s", msg)
                return JsonResponse(
                    {
                        "method": "sendMessage",
                        "chat_id": msg["chat"]["id"],
                        "text": emojize(
                            "Не удалось установить отправителя сообщения :confused:"
                        ),
                    }
                )

            request.user, _ = user_get_or_create(msg["from"])

            try:
                if msg.get("text") in commands:
                    return commands[msg["text"]](request, msg)

                elif msg.get("text", "")[:1] == "/":
                    return JsonResponse(
                        {
                            "method": "sendMessage",
                            "chat_id": msg["chat"]["id"],
                            "text": "No such command",
                        }
                    )

                elif "location" in msg:
                    return update_location(request, msg)

                else:
                    # TODO: save to the database to be able to reply!
                    return JsonResponse(
                        {
                            "method": "sendMessage",
                            "chat_id": msg["chat"]["id"],
                            "text": emojize(
                                "Обработка произвольных сообщений пока не реализована :confused:"
                            ),
                        }
                    )

            except Exception:
                logger.error("Failed processing %s:", msg, exc_info=True)
                return JsonResponse(
                    {
                        "method": "sendMessage",
                        "chat_id": msg["chat"]["id"],
                        "text": "Что-то пошло не так, сообщите администратору :exclamation:",
                    }
                )

        else:
            logger.error("Unsupported update: %s", update)
            return JsonResponse(
                {
                    "method": "sendMessage",
                    "chat_id": msg["chat"]["id"],
                    "text": emojize(
                        "Вы что-то сделали? Я не знаю как на это реагировать :confused:"
                    ),
                }
            )

    return botapi


def update_location(request, msg):

    try:
        lp = LatestPoint.objects.get(user=request.user)
    except LatestPoint.DoesNotExist:
        lp = LatestPoint(user=request.user)

    lp.point = "POINT(%s %s)" % (
        msg["location"]["longitude"],
        msg["location"]["latitude"],
    )
    lp.save()

    return JsonResponse(
        {
            "method": "sendMessage",
            "chat_id": msg["chat"]["id"],
            "text": "Ваше местоположение обновлено!",
        }
    )


def start_cmd(request, msg):

    info = (
        ":world_map: Карта сансёрферов - https://%s\n\n"
        ":lock: Чтобы получить доступ - отправь /login\n\n"
        ":round_pushpin: Чтобы поделиться со всеми своим местоположением просто отправь "
        "его сюда. После этого оно появится на :world_map:.\n\n"
        "Благодарим за интерес к использованию приложения! :pray:"
    ) % settings.TGAUTH_DOMAIN

    if msg["chat"]["type"] != "private":
        return JsonResponse(
            {
                "method": "sendMessage",
                "chat_id": msg["chat"]["id"],
                "text": "Эта команда должна быть отправлена личным сообщением, "
                "а не публично в группе.",
            }
        )

    return JsonResponse(
        {
            "method": "sendMessage",
            "chat_id": msg["chat"]["id"],
            "reply_markup": {
                "keyboard": [
                    [
                        {
                            "text": "Поделиться местоположением",
                            "request_location": True,
                        }
                    ]
                ],
                "resize_keyboard": True,
            },
            "text": emojize(
                ("Привет, @{username}!\n\n" "{info}").format(
                    username=request.user.username, info=info
                )
            ),
        }
    )


def login_cmd(request, msg):

    # XXX: is the bot suitable to be used in groups anyway?
    if msg["chat"]["type"] != "private":
        return JsonResponse(
            {
                "method": "sendMessage",
                "chat_id": msg["chat"]["id"],
                "text": "Эта команда должна быть отправлена личным сообщением, "
                "а не публично в группе.",
            }
        )

    user_get_or_create(msg["from"])

    return JsonResponse(
        {
            "method": "sendMessage",
            "chat_id": msg["chat"]["id"],
            "disable_web_page_preview": True,
            "text": (
                "Ссылка для входа на сайт (действует 10 минут):\n"
                "https://{domain}{url}"
            ).format(
                domain=settings.TGAUTH_DOMAIN,
                url=reverse(
                    "tgauth:login", args=[signer.sign(msg["from"]["id"])]
                ),
            ),
        }
    )


botapi_view = BotAPI(
    {"/login": login_cmd, "/start": start_cmd}, settings.TGAUTH_WEBHOOK_SUFFIX
)


def login_view(request, token):
    user = authenticate(token=token)
    if user is not None:
        login(request, user)
        return redirect(settings.LOGIN_REDIRECT_URL)
    else:
        raise Http404()
