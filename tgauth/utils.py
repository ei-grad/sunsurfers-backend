import emoji


def emojize(msg):
    return emoji.emojize(msg, use_aliases=True)
