#!/bin/bash

EXITCODE=0

./manage.py migrate
./manage.py loaddata initial_data
./manage.py collectstatic
./manage.py test || EXITCODE=$?
./manage.py behave -S -f allure || echo "Behavior tests are failed, but it is ok for now."

exit $EXITCODE
