import random

from time import time, sleep

from django.core.management.base import BaseCommand
from django.conf import settings

import requests

from tqdm import tqdm

from social_django.models import UserSocialAuth

from randomcoffee.views import get_week_start
from randomcoffee.models import Coffee


class Command(BaseCommand):
    help = "Compose randomcoffee pairs and send notifications"

    def add_arguments(self, parser):
        parser.add_argument(
            "--token",
            default=settings.RANDOMCOFFEE_BOT_TOKEN,
            help="API token to use (get from @BotFather)",
        )

    def handle(self, token, **kwargs):
        week = get_week_start()
        result = list(Coffee.objects.filter(week=week, partner=None))
        lucky_candidates = list(result)
        for _ in tqdm(range(len(result) // 2)):
            a = result.pop(random.randint(0, len(result) - 1))
            b = result.pop(random.randint(0, len(result) - 1))
            a.partner = b.user
            a.save()
            b.partner = a.user
            b.save()
            print(send_info(a, TPL_DEFAULT, token))
            print(send_info(b, TPL_DEFAULT, token))

        # odd number, last one left
        if result:
            unlucky = result[0]
            lucky_candidates.remove(unlucky)
            lucky = Coffee(
                week=week,
                user=lucky_candidates[
                    random.randint(0, len(lucky_candidates) - 1)
                ],
                partner=unlucky,
            )
            lucky.save()
            unlucky.partner = lucky.user
            unlucky.save()
            print(send_info(lucky, TPL_LUCKY, token))
            print(send_info(unlucky, TPL_DEFAULT, token))


TPL_DEFAULT = (
    "Привет!\nНастало время распределения пар на следующую неделю :slightly_smiling_face:."
    "Твой партнер по Random Coffee на следующей неделе - "
    "[{display_name}](tg://user?id={uid})."
)

TPL_LUCKY = (
    "Тебе крупно повезло! На этой неделе у тебя будет целых два партнера по Random Coffee!"
    "Второй партнер - [{display_name}](tg://user?id={uid})."
)


http = requests.Session()


def ratelimit(rps):
    prev_t = time()
    while True:
        yield
        dt = 1 / rps - (time() - prev_t)
        if dt > 0.0:
            sleep(dt)
        prev_t = time()


tgapi_rate_limiter = ratelimit(30)


def send_info(coffee, template, token):
    partner = UserSocialAuth.objects.get(
        user=coffee.partner, provider="telegram"
    )
    display_name = partner.extra_data["first_name"]
    if partner.extra_data.get("last_name"):
        display_name += " " + partner.extra_data["last_name"]
    text = template.format(display_name=display_name, uid=partner.uid)
    next(tgapi_rate_limiter)
    return http.post(
        "https://api.telegram.org/bot{}/sendMessage".format(token),
        json={
            "chat_id": coffee.chat_id,
            "text": text,
            "parse_mode": "markdown",
        },
    )
