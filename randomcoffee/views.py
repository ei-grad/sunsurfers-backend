from datetime import date, timedelta

from django.conf import settings
from django.http import JsonResponse

from tgauth.views import BotAPI
from tgauth.utils import emojize

from randomcoffee.models import Week, Coffee


def start_cmd(request, msg):
    info = (
        "Привет, {username}.\n\n"
        "Добро пожаловать! Я telegram-бот Sunsurfers Random Coffee.\n"
        ":sunny: :game_die: :coffee:\n\n"
        "Каждую неделю :calendar: участники случайным образом бьются на пары "
        ":two_men_holding_hands: :two_women_holding_hands: :man_and_woman_holding_hands:,"
        " связываются друг с другом :wave:, договариваются о :clock10: времени встречи.\n\n"
        "Встреча может проходить в формате созвона по видео :computer: :iphone: или "
        "оффлайн :coffee: - в кафе или любом другом удобном месте, если повезло оказаться в одном городе :airplane:.\n\n"
        "Обсуждать можно что угодно - от путешествий и йоги, до программирования и погоды, "
        "лишь бы тема была интересна обоим собеседникам. Цель Random Coffee - познакомиться "
        "с большим количеством человек и узнать друг о друге как можно больше."
        # TODO: дописать
    ).format(username=request.user.first_name)
    return reply(msg, info, {"text": CMD_ENLIST})


def get_week_start():
    today = date.today()
    week_start = today + timedelta(days=7 - today.weekday())
    week, created = Week.objects.get_or_create(date=week_start)
    if created:
        week.save()
    return week


def reply(msg, text, button=None):
    resp = {
        "method": "sendMessage",
        "chat_id": msg["chat"]["id"],
        "text": emojize(text),
    }
    if button is not None:
        resp["reply_markup"] = {
            "keyboard": [[button]],
            "resize_keyboard": True,
        }
    return JsonResponse(resp)


def enlist_cmd(request, msg):
    week = get_week_start()
    coffee, created = Coffee.objects.get_or_create(
        week=week, user=request.user, defaults={"chat_id": msg["chat"]["id"]}
    )
    if created:
        coffee.save()
        # TODO: если распределение уже было - выдавать пару сразу или писать "я постараюсь найти вам пару"
        return reply(
            msg,
            "Отлично! :+1:\nРаспределение пар производится в субботу, "
            "я пришлю тебе сообщение :slightly_smiling_face:.",
            {"text": CMD_REFUSE},
        )
    else:
        return reply(
            msg,
            "Ты уже в списке, просто подожди до субботы :slightly_smiling_face:.",
            {"text": CMD_REFUSE},
        )


def refuse_cmd(request, msg):
    week = get_week_start()
    res = Coffee.objects.filter(week=week, user=request.user)[:1]
    if res:
        coffee = res[0]
        if coffee.partner is not None:
            return reply(
                msg,
                "Нельзя отказываться после того как пары сформированы! :rage:\n"
                "Если не можешь участвовать - помоги своей паре найти другого собеседника.",
            )
        else:
            coffee.delete()
            return reply(
                msg,
                "Ок :pensive:, вычеркиваю.\nНапиши «{}» если передумаешь.".format(
                    CMD_ENLIST
                ),
                {"text": CMD_ENLIST},
            )
    else:
        return reply(
            msg,
            ":thinking_face: А ты и не регистрировался на следующую неделю.\n\n"
            "Напиши «{}» если передумаешь :slightly_smiling_face:.".format(
                CMD_ENLIST
            ),
            {"text": CMD_ENLIST},
        )


CMD_ENLIST = "Я участвую"
CMD_REFUSE = "Я хочу отменить участие"


botapi_view = BotAPI(
    {"/start": start_cmd, CMD_ENLIST: enlist_cmd, CMD_REFUSE: refuse_cmd},
    settings.RANDOMCOFFEE_WEBHOOK_SUFFIX,
)
