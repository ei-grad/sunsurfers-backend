from django.urls import path

from randomcoffee import views


app_name = "randomcoffee"
urlpatterns = [path("webhook/<token>", views.botapi_view, name="webhook")]
