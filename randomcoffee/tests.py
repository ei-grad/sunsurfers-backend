from django.test import TestCase
from django.conf import settings
from django.urls import reverse

from tgauth.views import user_get_or_create

from randomcoffee import views


class DummyRequest(object):
    def __init__(self, user):
        self.user = user


class TestCase(TestCase):

    USER_DATA = {"id": 2, "first_name": "Andrew", "username": "ei-grad"}

    def setUp(self):
        self.user, _ = user_get_or_create(self.USER_DATA)
        self.request = DummyRequest(self.user)


class TestCommands(TestCase):
    def test_start_cmd(self):
        response = views.start_cmd(self.request, {"chat": {"id": 1}})
        self.assertEqual(response.status_code, 200)

    def test_enlist_cmd(self):
        response = views.enlist_cmd(self.request, {"chat": {"id": 1}})
        self.assertEqual(response.status_code, 200)

    def test_refuse_cmd(self):
        response = views.refuse_cmd(self.request, {"chat": {"id": 1}})
        self.assertEqual(response.status_code, 200)


class TestView(TestCase):
    def test_start_cmd(self):
        response = self.client.post(
            reverse(
                "randomcoffee:webhook",
                args=(settings.RANDOMCOFFEE_WEBHOOK_SUFFIX,),
            ),
            {
                "message": {
                    "chat": {"id": 1},
                    "from": self.USER_DATA,
                    "text": "/start",
                }
            },
            content_type="application/json",
        )
        self.assertEqual(response.status_code, 200)
