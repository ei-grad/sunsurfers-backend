from django.conf import settings
from django.db import models


class Week(models.Model):
    date = models.DateField(unique=True)


class Coffee(models.Model):
    week = models.ForeignKey(Week, models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.CASCADE, related_name="+"
    )
    chat_id = models.BigIntegerField()
    partner = models.ForeignKey(
        settings.AUTH_USER_MODEL, models.CASCADE, null=True, related_name="+"
    )
